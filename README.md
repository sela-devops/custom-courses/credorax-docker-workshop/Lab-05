# Docker Workshop
Lab 05: Building more complex images

---

## Instructions

 - Ensure you are in the previous created folder path (~/lab-04):
```
$ pwd
```

 - Create file "index.html" next to the Dockerfile with the content below:
```
<h1>Python App</h1>
```

 - You can create the "index.html" file by run the command:
```
echo "<h1>Python App</h1>" > index.html
```

 - Check your current directory before continue:
```
$ ls -l

total 2
-rw-r--r-- 1 docker 1049089 123 Jun 13 21:37 Dockerfile
-rw-r--r-- 1 docker 1049089  19 Jun 13 21:54 index.html
```

 - Edit the dockerfile to perform the following changes:
 
   - Expose the port 5000
   - Replace the CMD instruction for ENTRYPOINT to avoid override the command from the docker run command
   - Add a build argument to set the application port during the build
   - Add a environment variable to override the build argument at runtime
   - Set the app directory as the container working directory
   - Copy the index file to the container using the COPY instruction  

<br />

 - The Dockerfile will contain the following:
```
FROM selaworkshops/alpine:3.4
RUN apk-install python
ARG port=5000
ENV port=$port
WORKDIR /app
COPY index.html /app/
ENTRYPOINT python -m SimpleHTTPServer $port
EXPOSE $port
```

 - Build the new image without build arguments:
```
$ docker build -t python-app:no-arg .
```

 - Run the created image without passing environment variables:
```
$ docker run -it --name app-port-5000 -p 5000:5000 python-app:no-arg
```

 - Browse to the application:
```
http://localhost:5000
```

 - Exit from the running container:
```
$ (Ctrl + C)
```

 - Build the new image passing a build argument:
```
$ docker build --build-arg port=5001 -t python-app:build-arg .
```

 - Run the created image without passing environment variables:
```
$ docker run -it --name app-port-5001 -p 5001:5001 python-app:build-arg
```

 - Browse to the application:
```
http://localhost:5001
```

 - Exit from the running container:
```
$ (Ctrl + C)
```

 - Run the created image passing the port 5002 as environment variable:
```
$ docker run -it --name app-port-5002 -p 5002:5002 -e "port=5002" python-app:build-arg
```

 - Browse to the application:
```
http://localhost:5002
```

 - Exit from the running container:
```
$ (Ctrl + C)
```

## Cleanup

 - Remove the created containers:
```
$ docker rm -f app-port-5000 app-port-5001 app-port-5002
```

 - Remove the created images:
```
$ docker rmi -f python-app:no-arg python-app:build-arg
```

